define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'taxi/tianmai_order/index' + location.search,
                    add_url: 'taxi/tianmai_order/add',
                    edit_url: 'taxi/tianmai_order/edit',
                    del_url: 'taxi/tianmai_order/del',
                    multi_url: 'taxi/tianmai_order/multi',
                    import_url: 'taxi/tianmai_order/import',
                    table: 'taxi_tianmai_order',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'car_number', title: __('Car_number'), operate: 'LIKE'},
                        {field: 'car_color', title: __('Car_color'), operate: 'LIKE'},
                        {field: 'drvier_number', title: __('Drvier_number'), operate: 'LIKE'},
                        {field: 'user_up_car_time', title: __('User_up_car_time'), operate: 'LIKE'},
                        {field: 'user_up_car_latitude', title: __('User_up_car_latitude'), operate: 'LIKE'},
                        {field: 'user_up_car_longitude', title: __('User_up_car_longitude'), operate: 'LIKE'},
                        {field: 'user_down_car_time', title: __('User_down_car_time'), operate: 'LIKE'},
                        {field: 'user_down_car_latitude', title: __('User_down_car_latitude'), operate: 'LIKE'},
                        {field: 'user_down_car_longitude', title: __('User_down_car_longitude'), operate: 'LIKE'},
                        {field: 'operating_mileage', title: __('Operating_mileage'), operate:'BETWEEN'},
                        {field: 'deadhead_mileage', title: __('Deadhead_mileage'), operate:'BETWEEN'},
                        {field: 'price', title: __('Price'), operate:'BETWEEN'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'taxi/tianmai_order/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'taxi/tianmai_order/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'taxi/tianmai_order/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});