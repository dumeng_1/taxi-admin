define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'taxi/invoices/index' + location.search,
                    add_url: 'taxi/invoices/add',
                    edit_url: 'taxi/invoices/edit',
                    del_url: 'taxi/invoices/del',
                    multi_url: 'taxi/invoices/multi',
                    import_url: 'taxi/invoices/import',
                    table: 'taxi_invoices',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'order_no', title: __('Order_no'), operate: 'LIKE'},
                        {field: 'payer_tel_phone', title: __('Payer_tel_phone'), operate: 'LIKE'},
                        {field: 'm_short_name', title: __('M_short_name'), operate: 'LIKE'},
                        {field: 'payer_bank_name', title: __('Payer_bank_name'), operate: 'LIKE'},
                        {field: 'invoice_amount', title: __('Invoice_amount'), operate: 'LIKE'},
                        {field: 'payer_bank_account', title: __('Payer_bank_account'), operate: 'LIKE'},
                        {field: 'user_id', title: __('User_id'), operate: 'LIKE'},
                        {field: 'user_mobile_no', title: __('User_mobile_no'), operate: 'LIKE'},
                        {field: 'apply_id', title: __('Apply_id'), operate: 'LIKE'},
                        {field: 'trade_time', title: __('Trade_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'payer_address', title: __('Payer_address'), operate: 'LIKE'},
                        {field: 'sub_short_name', title: __('Sub_short_name'), operate: 'LIKE'},
                        {field: 'payer_name', title: __('Payer_name'), operate: 'LIKE'},
                        {field: 'payer_register_no', title: __('Payer_register_no'), operate: 'LIKE'},
                        {field: 'user_email', title: __('User_email'), operate: 'LIKE'},
                        {field: 'is_retry_open', title: __('Is_retry_open'), operate: 'LIKE'},
                        {field: 'invoice_order_sn', title: __('Invoice_order_sn'), operate: 'LIKE'},
                        {field: 'invoice_ticket_code', title: __('Invoice_ticket_code'), operate: 'LIKE'},
                        {field: 'invoice_ticket_sn', title: __('Invoice_ticket_sn'), operate: 'LIKE'},
                        {field: 'invoice_ticket_date', title: __('Invoice_ticket_date'), operate: 'LIKE'},
                        {field: 'invoice_ticket_tax_amount', title: __('Invoice_ticket_tax_amount'), operate:'BETWEEN'},
                        {field: 'invoice_ticket_total_amount_has_tax', title: __('Invoice_ticket_total_amount_has_tax'), operate:'BETWEEN'},
                        {field: 'invoice_ticket_total_amount_no_tax', title: __('Invoice_ticket_total_amount_no_tax'), operate:'BETWEEN'},
                        {field: 'invoice_order_id', title: __('Invoice_order_id'), operate: 'LIKE'},
                        {field: 'status', title: __('Status')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'taxi/invoices/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'taxi/invoices/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'taxi/invoices/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});