define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'swain/command/index' + location.search,
                    add_url: 'swain/command/add',
                    edit_url: 'swain/command/edit',
                    del_url: 'swain/command/del',
                    multi_url: 'swain/command/multi',
                    import_url: 'swain/command/import',
                    table: 'swain_command',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'addon_name', title: __('插件名称')},
                        {field: 'table', title: __('表名')},
                        {field: 'name', title: __('控制器名')},
                        {field: 'path', title: __('路径')},
                        {field: 'status', title: __('状态'),searchList:{'success':'成功','fail':'失败'},formatter: Table.api.formatter.status},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'base/command/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'base/command/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'base/command/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();

            $(document).on('click', ".btn-command", function () {
                var form = $(this).closest("form");
                var textarea = $("textarea[rel=command]", form);
                textarea.val('');
                Fast.api.ajax({
                    url: "swain/command/getCommand",
                    data: form.serialize(),
                }, function (data, ret) {
                    textarea.val(data.command);
                    return false;
                });
            });

            $(document).on('click', ".btn-execute", function () {
                var form = $(this).closest("form");
                var textarea = $("textarea[rel=result]", form);
                textarea.val('');
                Fast.api.ajax({
                    url: "swain/command/doCommand",
                    data: form.serialize(),
                }, function (data, ret) {
                    textarea.val(data.result);
                    window.parent.$(".toolbar .btn-refresh").trigger('click');
                    top.window.Fast.api.refreshmenu();
                    return false;
                }, function () {
                    window.parent.$(".toolbar .btn-refresh").trigger('click');
                });
            });

        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});