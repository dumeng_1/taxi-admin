<?php

return [
    'autoload' => false,
    'hooks' => [
        'app_init' => [
            'epay',
            'swain',
        ],
        'testhook' => [
            'golden',
            'swain',
            'taxi',
            'wechatv3',
        ],
        'invoice_request' => [
            'golden',
        ],
    ],
    'route' => [],
    'priority' => [],
];
