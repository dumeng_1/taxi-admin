<?php

namespace app\api\controller;

use app\common\controller\Api;

/**
 * 测试接口
 */
class Rest extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 首页
     *
     */
    public function rest()
    {

        $postData = $this->request->param();

        $this->success('请求成功', $postData);


    }
}
