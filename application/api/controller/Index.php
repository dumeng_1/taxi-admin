<?php

namespace app\api\controller;

use addons\epay\library\Service;
use addons\golden\library\Golden;
use addons\taxi\model\Invoices;
use app\common\controller\Api;
use think\Log;
use Yansongda\Pay\Gateways\Alipay\Support;
use Yansongda\Pay\Pay;

/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 首页
     *
     */
    public function index()
    {
        vendor('aop.AopClient');
        vendor('aop.request.AlipayEbppInvoiceInfoSendRequest');
        vendor('aop.AopCertClient');

        $aop = new \AopClient();

        $aop->rsaPrivateKey = "MIIEpQIBAAKCAQEApdfdqaUlcU2hj6/4aDlQPdIaAU/rKkIq23GKOZeRjtxicgfnlMnsIeFA/cPMdaWhxVhjascj+R1Ho2Qt49BOqNq1M3oAx/KbtIrkGd4J/NIUN31620oIUelPwNRHdUfxqsRPIEvxMKN7BzvpkYxsT5IO2U3ab0ws+d9qf2Vtit5rbLoPct+wgAsbKwI3y0qm98QaN+obIfj7L//Vp2vgkO55Fx2JxHB5LDMKMZQ6hhmYMg6nDSxmF8H75v+pLV93j8tFOHmaQyPRBl1TmDLD3WjED6Y47xBkl1KkzjT3Edu+G0K+WMgPGRAdYT3cyK5xobzTlZijtmwykMXIPa9QlwIDAQABAoIBAQCeJoHywlM3fmGAZb4MngNAQPidobnP3bOC+v5mt3yOcOnMdm9IO1rcYDObC7oRwvbG++gqrmdGq75iOSlt43rvmUlNIp5WDqyfegfWByEdILU4yYd8UVgnzMdKSuUuGYKDZ5tRYDpMOMoc9uxg6TlslAtm/NA+bLVDP+QOQwo4byGPXVzAQ06SdgcTPIPgRChMu71ELqHz/8VJfqdwZIsVz4DFoZO3qbw8gorFk+CbNyplWw6A14hh5ni/Qb3tUfnBGqH+77rprrLbJUT1j182mHqtRNIvDAmAHTz+6E9ieQSh2mm2DLz0jidM+yYf8M9iQH/+q1gCsMOuwm1l/JgpAoGBAORc9OIaGZNuIJaPevTkDH1k9nDklcTG2tyA55Fc8y4KAAZeGHLkWcRlGB2jz9i0UbX5XU1ZVYgw+YhcITKvRACySRYTmsWjL7RUUJi8USe9fZRvtzspiGFid3OexT5yiTcdu4/xwMKVdWPBsDAXp7GuTJprag4bG3HwBS9A025FAoGBALnp8j42XIr/GH/sq+4WnX2pHolnd32GeAWid2naU2yTKyB2XXnE0pMU2QkakS7Yk8s/cxv5JZYqyNmhBOvaPDim42UKGUvTmI08LEdE36cedz8E739Tn5RtpZ8igvwBdAvKIPh8hgTQJDGsjYsGRcffaqjfF9wp3jvKTLL9Gs8rAoGBAKSzkn7cOiQF4oUUeKbVL1jlD4T+qDIjBcjRQ7Koqk9DMR9mkBWVMlUeYJVIB2kRLHmlnzBhRPoT8HTzlZH7rH0gGOxOOWtftdFGmlZ+Q734wfDj3fBSlSU/ok2GblYKv4I79IHt54uvxdmr4UneVbklr+QToosH6/RyGxsokxVNAoGACVyCWieamVUnAqoELkTtQTqW0wxms+dle8MmiCn6MWlnrobHRi5m/Aj8tLylutok9wMG5M2y2tDktDCrcsTWa3Pb12aex3asI9B32k7ZhCzAjGfPN3YafvrWcCDov4/DLCTNbDW4+d0RNX8e0XVLZjkVwdMZ/HgPPKt/GTQteWkCgYEAjno7OA5TpgNuBeLSehIJlcghNZfQQzsxBjC1HuTDi9wvHy2SmiYnXW3K5ZlpmFZRfRbq6rXlHbcqNrGVjusG0uzvcYYluwuwwFikM1j1RabEHdbYebjkys1BBddKoXMHhmGyL63KqpnDmgDehLZlsycQ+oaAM3qs8ycbhHLt5Bo=";
        $aop->alipayrsaPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlqesLO+yf/VUuAsneQTAJSL+5TzVdlddXr1a+Z9SxcJYIyk8H4bKbVDLOSHFuogVBMhVmCnuC4Y25RWU5BOQsOcCw2TyKkfZ4YV713ZxixIr02Xz82AqiHIv5MpeMMLlyUpm9RUbJTSwLMoBt14zgJ/Fccyv5WdlBCd0Dwm4ofw7+Dwcio/ImEO33o6qGnvECh1Ls8iPTen0Hbqo2FpoVFQ3G0WG/lQ++TfD0LX1YDjnW+9tOTAey9IiEZP7ZiB9Gfi1x+xlPbNB9DqU8JFYoyXM9dqTULhEZqHKQa8PEd2exWWIKDJGGjLCdgy7OhWyZWY+wr4s0R4in+G9ilPSXQIDAQAB";


        $result['resultMsg'] = "申请成功";
        $result['resultCode'] = "APPLY_SUCCESS";
        $result['sign'] = $aop->generateSign($result,'RSA2');
        return json($result);

        die();

        $invoice = Invoices::get(['invoice_order_sn'=>'6855100083302612238']);
       
        
        $config = Service::getConfig('alipay');
        $pay = Pay::alipay($config);



        vendor('aop.AopClient');
        vendor('aop.request.AlipayEbppInvoiceInfoSendRequest');
        vendor('aop.AopCertClient');

        $aop = new \AopClient ();


        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->appId = '2021002133635801';
        $aop->rsaPrivateKey = "MIIEpQIBAAKCAQEApdfdqaUlcU2hj6/4aDlQPdIaAU/rKkIq23GKOZeRjtxicgfnlMnsIeFA/cPMdaWhxVhjascj+R1Ho2Qt49BOqNq1M3oAx/KbtIrkGd4J/NIUN31620oIUelPwNRHdUfxqsRPIEvxMKN7BzvpkYxsT5IO2U3ab0ws+d9qf2Vtit5rbLoPct+wgAsbKwI3y0qm98QaN+obIfj7L//Vp2vgkO55Fx2JxHB5LDMKMZQ6hhmYMg6nDSxmF8H75v+pLV93j8tFOHmaQyPRBl1TmDLD3WjED6Y47xBkl1KkzjT3Edu+G0K+WMgPGRAdYT3cyK5xobzTlZijtmwykMXIPa9QlwIDAQABAoIBAQCeJoHywlM3fmGAZb4MngNAQPidobnP3bOC+v5mt3yOcOnMdm9IO1rcYDObC7oRwvbG++gqrmdGq75iOSlt43rvmUlNIp5WDqyfegfWByEdILU4yYd8UVgnzMdKSuUuGYKDZ5tRYDpMOMoc9uxg6TlslAtm/NA+bLVDP+QOQwo4byGPXVzAQ06SdgcTPIPgRChMu71ELqHz/8VJfqdwZIsVz4DFoZO3qbw8gorFk+CbNyplWw6A14hh5ni/Qb3tUfnBGqH+77rprrLbJUT1j182mHqtRNIvDAmAHTz+6E9ieQSh2mm2DLz0jidM+yYf8M9iQH/+q1gCsMOuwm1l/JgpAoGBAORc9OIaGZNuIJaPevTkDH1k9nDklcTG2tyA55Fc8y4KAAZeGHLkWcRlGB2jz9i0UbX5XU1ZVYgw+YhcITKvRACySRYTmsWjL7RUUJi8USe9fZRvtzspiGFid3OexT5yiTcdu4/xwMKVdWPBsDAXp7GuTJprag4bG3HwBS9A025FAoGBALnp8j42XIr/GH/sq+4WnX2pHolnd32GeAWid2naU2yTKyB2XXnE0pMU2QkakS7Yk8s/cxv5JZYqyNmhBOvaPDim42UKGUvTmI08LEdE36cedz8E739Tn5RtpZ8igvwBdAvKIPh8hgTQJDGsjYsGRcffaqjfF9wp3jvKTLL9Gs8rAoGBAKSzkn7cOiQF4oUUeKbVL1jlD4T+qDIjBcjRQ7Koqk9DMR9mkBWVMlUeYJVIB2kRLHmlnzBhRPoT8HTzlZH7rH0gGOxOOWtftdFGmlZ+Q734wfDj3fBSlSU/ok2GblYKv4I79IHt54uvxdmr4UneVbklr+QToosH6/RyGxsokxVNAoGACVyCWieamVUnAqoELkTtQTqW0wxms+dle8MmiCn6MWlnrobHRi5m/Aj8tLylutok9wMG5M2y2tDktDCrcsTWa3Pb12aex3asI9B32k7ZhCzAjGfPN3YafvrWcCDov4/DLCTNbDW4+d0RNX8e0XVLZjkVwdMZ/HgPPKt/GTQteWkCgYEAjno7OA5TpgNuBeLSehIJlcghNZfQQzsxBjC1HuTDi9wvHy2SmiYnXW3K5ZlpmFZRfRbq6rXlHbcqNrGVjusG0uzvcYYluwuwwFikM1j1RabEHdbYebjkys1BBddKoXMHhmGyL63KqpnDmgDehLZlsycQ+oaAM3qs8ycbhHLt5Bo=";
        $aop->alipayrsaPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlqesLO+yf/VUuAsneQTAJSL+5TzVdlddXr1a+Z9SxcJYIyk8H4bKbVDLOSHFuogVBMhVmCnuC4Y25RWU5BOQsOcCw2TyKkfZ4YV713ZxixIr02Xz82AqiHIv5MpeMMLlyUpm9RUbJTSwLMoBt14zgJ/Fccyv5WdlBCd0Dwm4ofw7+Dwcio/ImEO33o6qGnvECh1Ls8iPTen0Hbqo2FpoVFQ3G0WG/lQ++TfD0LX1YDjnW+9tOTAey9IiEZP7ZiB9Gfi1x+xlPbNB9DqU8JFYoyXM9dqTULhEZqHKQa8PEd2exWWIKDJGGjLCdgy7OhWyZWY+wr4s0R4in+G9ilPSXQIDAQAB";
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'utf-8';
        $aop->format = 'json';


        $biz_content = [];

//        $ali_data['app_id'] = "2021002133635801";
//        $ali_data['method'] = 'alipay.ebpp.invoice.info.send';
//        $ali_data['format'] = 'JSON';
//        $ali_data['charset'] = 'utf-8';
//        $ali_data['sign_type'] = 'RSA2';
//        $ali_data['timestamp'] = date('Y-m-d H:i:s');
//        $ali_data['version'] = '1.0';

        $biz_content['m_short_name'] = $invoice->m_short_name;
        $biz_content['sub_m_short_name'] = $invoice->sub_short_name;

        // $invoice_info_info['apply_id'] = $invoice->apply_id;
        $invoice_info_info['user_id'] = $invoice->user_id;
        $invoice_info_info['invoice_code'] = $invoice->invoice_ticket_code;
        $invoice_info_info['invoice_no'] = $invoice->invoice_ticket_sn;
        $invoice_info_info['invoice_date'] = date('Y-m-d',strtotime($invoice->invoice_ticket_date));
        $invoice_info_info['sum_amount'] = $invoice->invoice_ticket_total_amount_has_tax;
        $invoice_info_info['ex_tax_amount'] = $invoice->invoice_ticket_total_amount_no_tax;
        $invoice_info_info['tax_amount'] = $invoice->invoice_ticket_tax_amount;

        $invoice_content['item_name'] = "餐饮费";
        $invoice_content['item_no'] = "1010101990000000000";
        $invoice_content['row_type'] = 0;
        $invoice_content['item_ex_tax_amount'] = $invoice->invoice_ticket_total_amount_no_tax;
        $invoice_content['item_tax_rate'] = '0.00';
        $invoice_content['item_tax_amount'] = $invoice->invoice_ticket_tax_amount;
        $invoice_content['item_sum_amount'] = $invoice->invoice_ticket_total_amount_has_tax;

        $invoice_title['title_name'] = $invoice->payer_name;
        $invoice_title['payer_register_no'] = $invoice->payer_register_no;

        $invoice_info_info['invoice_title'] = $invoice_title;
        $invoice_info_info['payee_register_no'] = "140301195503104110";
        $invoice_info_info['payee_register_name'] = "开发平台外部专测";
        $invoice_info_info['out_invoice_id'] = $invoice->invoice_order_id;
        $invoice_info_info['file_download_type'] = "PDF";
        $invoice_info_info['file_download_url'] = $invoice->invoice_pdf_url;
        $invoice_info_info['out_trade_no'] =$invoice->invoice_order_sn;
        $invoice_info_info['invoice_type'] = 'BLUE';
        $invoice_info_info['invoice_kind'] = 'PLAIN';

        $invoice_info_info['invoice_content'][] = $invoice_content;
        $biz_content['invoice_info_list'][] = $invoice_info_info;

        
        




//        $ali_data['biz_content'] = json_encode($biz_content);


//        $sign = Support::generateSign($ali_data);
//        $ali_data['sign'] = $sign;

//        echo json_encode($biz_content['invoice_info_list']);

        $url = "https://openapi.alipay.com/gateway.do";

//        $result = $this->postUrl($url,$ali_data);
//        $result = json_decode($result,true);



        $request = new \AlipayEbppInvoiceInfoSendRequest ();
        $request->setBizContent(json_encode($biz_content));

        //Log::write(json_encode($biz_content));


        $result = $aop->execute( $request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;



        if(!empty($resultCode)&&$resultCode == 10000){
            $invoice->status = 3;

        } else {
            $invoice->status = 4;
        }
        $invoice->save();
        die();
//        $url = "/invoice/easy_invoice";


//        $golden = Golden::instance();
//        $url = "/invoice/easy_invoice";
//        $data['seller_taxpayer_num'] = "140301195503104110";
//        $data['title_type'] = 1;
//        $data['buyer_title'] = '河南渡梦信息科技有限公司';
//        $data['buyer_taxpayer_num'] = "91410105MA44BFHX7U";
//        $data['order_id'] = "SN".time()."";
//        $data['total_price_has_tax'] = 1;
//        $data['callback_url'] = cdnurl('/addons/taxi/notify/goldenAlipayNotify',true);
//        $data['buyer_email'] = "839113670@qq.com";
//
//        $result = $golden->httpRequest($url,$data);
//
//        $result = json_decode($result,true);

//        echo $result['data']['invoice_id'];
//        var_dump($result);

//        if($result)
//        $this->success('请求成功',$result);


//        $seller_taxpayer_num = $data['seller_taxpayer_num'];
//        $url = "/invoice/query";
//        $order_data['seller_taxpayer_num'] = $seller_taxpayer_num;
//        $order_data['order_sn'] = $result['data']['order_sn'];
//
//        $result = $golden->httpRequest($url,$order_data);
//        $result = json_decode($result,true);

        return json($result);
    }

    public function getInvoice()
    {
        $url = "/invoice/query";
        $data['seller_taxpayer_num'] = "140301195503104110";
        $data['order_sn'] = "6855046326703406333";

    }


    public function postUrl($url, $post_data = '', $timeout = 5){//curl
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_POST, 1);
        if($post_data != ''){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $file_contents = curl_exec($ch);
        curl_close($ch);
        return $file_contents;
    }


}
