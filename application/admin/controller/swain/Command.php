<?php

namespace app\admin\controller\swain;

use addons\swain\command\SwainCommand;
use app\common\controller\Backend;
use think\Config;
use think\console\Input;
use think\console\Output;
use think\Exception;
use think\Hook;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Command extends Backend
{
    
    /**
     * Command模型对象
     * @var \app\admin\model\swain\Command
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\swain\Command;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    public function add()
    {

        $tableList = [];
        $addonList = [];

        $list = \think\Db::query("SHOW TABLES");
        foreach ($list as $key => $row) {
            $tableList[reset($row)] = reset($row);
        }

        $list = get_addon_list();
        foreach ($list as $key => $row) {
            $addonList[reset($row)] = reset($row);
        }


        $this->view->assign("tableList", $tableList);
        $this->view->assign("addonList", $addonList);
        return $this->view->fetch();
    }

    /**
     * 获取命令行
     */
    public function getCommand()
    {
        // table addon_name path name

        $params = $this->request->post();
        extract($params);


        $command = "";
        $command .= "php think base:Curd ";

        $command .= " --name $name  --table $table  --addon_name $addon_name --path $path";

        $data['command'] = $command;

        $this->success('','',$data);

    }

    public function doCommand()
    {
        $params = $this->request->post();
        extract($params);
        if(isset($params['commandtype']))
        {
            unset($params['commandtype']);
        }
        $SwianCommand = new SwainCommand();

        /*替换table前缀*/


        $input_data[] ="--name=$name";
        $input_data[] ="--table=$table";
        $input_data[] ="--addon_name=$addon_name";
        $input_data[] ="--path=$path";

        $command = \app\admin\model\swain\Command::create($params);

        $input = new Input($input_data);
        $output = new Output();

        try {
            $SwianCommand->run($input,$output);
            $command->status = "success";
            $command->save();
            $this->success('操作成功','',['result'=>"Success"]);
        }
        catch (Exception $e)
        {
            $command->status = "fail";
            $command->save();
            $this->error('','',$e->getMessage());
        }

    }
    

}
