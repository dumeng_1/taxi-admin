<?php

namespace app\admin\model\taxi;

use think\Model;
use traits\model\SoftDelete;

class Invoices extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'taxi_invoices';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'trade_time_text'
    ];
    

    



    public function getTradeTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['trade_time']) ? $data['trade_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setTradeTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
