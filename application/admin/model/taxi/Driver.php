<?php

namespace app\admin\model\taxi;

use think\Model;
use traits\model\SoftDelete;

class Driver extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'taxi_driver';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [

    ];
    

    







}
