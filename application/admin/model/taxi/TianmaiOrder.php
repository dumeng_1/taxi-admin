<?php

namespace app\admin\model\taxi;

use think\Model;
use traits\model\SoftDelete;

class TianmaiOrder extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'taxi_tianmai_order';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'user_up_car_time_text',
        'user_down_car_time_text'
    ];
    

    



    public function getUserUpCarTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['user_up_car_time']) ? $data['user_up_car_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getUserDownCarTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['user_down_car_time']) ? $data['user_down_car_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setUserUpCarTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setUserDownCarTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
