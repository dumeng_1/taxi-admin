<?php

return [
    'Car_number'              => '车辆号牌',
    'Car_color'               => '车牌颜色代码',
    'Drvier_number'           => '驾驶员从业资格证号',
    'User_up_car_time'        => '乘客上车时间',
    'User_up_car_latitude'    => '乘客上车纬度',
    'User_up_car_longitude'   => '乘客上车经度',
    'User_down_car_time'      => '乘客下车时间',
    'User_down_car_latitude'  => '乘客下车纬度',
    'User_down_car_longitude' => '乘客下车经度',
    'Operating_mileage'       => '营运里程',
    'Deadhead_mileage'        => '空驶里程',
    'Price'                   => '营运收入金额'
];
