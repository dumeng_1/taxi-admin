<?php

return [
    'Company_id' => '公司id',
    'Name'       => '司机姓名',
    'Work_num'   => '司机工号',
    'Idcard'     => '司机身份证',
    'Car_number' => '司机车牌'
];
