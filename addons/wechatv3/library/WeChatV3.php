<?php
/*
@auth:raven
@date:2021/9/29 19:28
@remark:
*/

namespace addons\wechatv3\library;

class WeChatV3
{


    /**
     * @name:获取签名字符串
     * @param $method
     * @param $url
     * @param $time
     * @param $nonce
     * @param $body
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/29 19:56
     */
    public function getSignStr($method,$url,$time,$nonce,$body)
    {
        $sign_str = "$method\n$url\n$time\n$nonce\n$body\n";
        return $sign_str;
    }
    
    /**
     * @name:计算签名
     * @param $str
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/29 19:38
     */
    public function getSign($str)
    {
        $signature = "";
        $private_key = $this->getPrivateKey();
        openssl_sign($str, $signature, $private_key, 'sha256WithRSAEncryption');
        openssl_free_key($private_key);
        $sign = base64_encode($signature);
        return $sign;
    }

    /**
     * @name:获取私钥
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/29 19:43
     */
    public function getPrivateKey()
    {
        $private_key_path = $this->getPrivateKeyPath();
        $privateKey = file_get_contents($private_key_path);
        $key = openssl_get_privatekey($privateKey);
        return $key;
    }

    /**
     * @name:获取私钥路径
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/29 19:43
     */
    private function getPrivateKeyPath()
    {
        $path = ADDON_PATH.DS.'wechatv3'.DS.'certs'.DS.'apiclient_key.pem';
        return $path;
    }

    /**
     * @name:获取头部认证
     * @param $sign
     * @param $time
     * @param $nonce
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/29 19:49
     */
    public function getAuthorization($sign,$time,$nonce)
    {
        $auth = 'Authorization: WECHATPAY2-SHA256-RSA2048 mchid="'. get_addon_config('wechatv3')['mchid'] .'",nonce_str="'.  $nonce.'",signature="'.$sign .'",timestamp="'. $time  .'",serial_no="'.get_addon_config('wechatv3')['serial_no'].'"';
        return $auth;
    }

    /**
     * @name:获取微信顶部信息
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/29 20:14
     */
    public function getWechatpaySerial()
    {
        $serial = "Wechatpay-Serial:".get_addon_config('wechatv3')['pingtai_serial_no'];
        return $serial;
    }

    /**
     * @name:用平台证书加密字符串
     * @param $str
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/29 20:09
     */
    public static function getEncrypt($str)
    {
        //$str是待加密字符串
        $public_key_path = ROOT_PATH .DS. 'addons'.DS.'wechatv3'.DS.'certs/pingtai.crt';
        $public_key = file_get_contents($public_key_path);

        $encrypted = '';
        if (openssl_public_encrypt($str, $encrypted, $public_key, OPENSSL_PKCS1_OAEP_PADDING)) {
            //base64编码
            $sign = base64_encode($encrypted);
        } else {
            throw new Exception('encrypt failed');
        }
        return $sign;
    }

    /**
     * @name:获取随机字符串
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/29 19:57
     */
    public function getNonce()
    {
        return md5(time());
    }

    public function requestUrl($url,$data,$header = [])
    {
        $curl = curl_init();

        $header[] = 'User-Agent:Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/93.0.4577.82';
        $header[] = 'Content-Type:'.'application/json';

        if (!empty($header)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_HEADER, 0);//返回response头部信息
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        if (!empty($data)) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $output = curl_exec($curl);

        curl_close($curl);
        return $output;
    }



}