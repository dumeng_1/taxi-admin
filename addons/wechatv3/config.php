<?php

return [
    [
        //配置唯一标识
        'name'    => 'v3key',
        //显示的标题
        'title'   => '商户私钥',
        //类型
        'type'    => 'string',
        //数据字典
        'content' => [
        ],
        //值
        'value'   => 'dcf1493df2e967eb062a03cfded18b16',
        //验证规则 
        'rule'    => 'required',
        //错误消息
        'msg'     => '',
        //提示消息
        'tip'     => '',
        //成功消息
        'ok'      => '',
        //扩展信息
        'extend'  => ''
    ],
    [
        'name'    => 'mchid',
        'title'   => '商户号',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '1608007797',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => 'serial_no',
        'title'   => '商户API证书',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '6E469BBC2117693AA9853F2D98E4C661D42BE37E',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
    [
        'name'    => 'pingtai_serial_no',
        'title'   => '商户平台证书',
        'type'    => 'string',
        'content' => [
        ],
        'value'   => '21A02B8AA97B87A96D3A1DA1DBEFD8034DA52672',
        'rule'    => 'required',
        'msg'     => '',
        'tip'     => '',
        'ok'      => '',
        'extend'  => ''
    ],
];
