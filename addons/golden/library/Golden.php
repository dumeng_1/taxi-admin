<?php
/*
@auth:raven
@date:2021/7/8 19:04
@remark:
*/

namespace addons\golden\library;

class Golden
{
    protected $config = [];
    protected static $instance;
    public $params = [];
    public $error = "";
    public $sign = "";
    private $env;
    private $appkey;
    private $appsecret;
    private $version;

    public $baseUrl = [
        "test" => "https://openapi-test.wetax.com.cn",
        "prod" => "https://openapi.wetax.com.cn"
    ];

    public function __construct()
    {
        $config = get_addon_config('golden');
        $this->setAppkey($config['appkey']);
        $this->setAppsecret($config['appsecret']);
        $this->setEnv($config['env']);
        $this->setVerison($config['version']);
    }

    /**
     * 单例
     * @param array $options 参数
     * @return Golden
     */
    public static function instance($options = [])
    {
        if (is_null(self::$instance)) {
            self::$instance = new static($options);
        }

        return self::$instance;
    }

    /**
     * 设置签名
     * @param string $sign
     * @return Golden
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * 设置参数
     * @param array $param
     * @return Alisms
     */
    public function setParam(array $param = [])
    {
        $this->param = array_filter($param);
        return $this;
    }

    /**
     * @param $env
     * @throws \Exception
     */
    public function setEnv($env)
    {
        if( !in_array($env, ['test', 'prod']) ) throw new \Exception("env must be test or prod");
        $this->env = $env;
    }

    /**
     * @return string
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * @return string
     */
    public function getAppkey()
    {
        return $this->appkey;
    }

    /**
     * @return string
     */
    public function getAppsecret()
    {
        return $this->appsecret;
    }

    /**
     * @param $appkey
     */
    public function setAppkey($appkey)
    {
        $this->appkey = $appkey;
    }

    /**
     * @param $appsecret
     */
    public function setAppsecret($appsecret)
    {
        $this->appsecret = $appsecret;
    }

    /**
     * @return mixed
     */
    public function getVerison()
    {
        return $this->verison;
    }

    /**
     * @param mixed $ver
     */
    public function setVerison($ver)
    {
        $this->verison = $ver;
    }

    /**
     * @return array
     */
    public function getBaseUrl()
    {
        return $this->baseUrl[$this->getEnv()];
    }

    /**
     * @param array $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @param $timestamp
     * @param array $data
     * @return Golden
     */
    public function setSign($timestamp, array $data = [])
    {
        $originStr = $this->getAppkey() . $timestamp;
        ksort($data);

        $encodeStr = rawurlencode(call_user_func(function () use ($data) {
                $str = "";
                foreach ($data as $k => $v) {
                    if (is_array($v)) {
                        $v = json_encode($v, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
                    }
                    $str .= $k . '=' . $v . '&';
                }
                $str = rtrim($str, '&');
                return $str;
            })
        );
        $originStr .= $encodeStr . $this->getAppsecret();
        $sign = strtoupper(md5($originStr));
        $this->sign = $sign;
        return $this;
    }

    public function httpRequest($url, array $data)
    {
        unset($data['url']);

        $timestamp = time();
        $this->setSign($timestamp,$data);
        $url = $this->getBaseUrl() . $url;
        $url .= "?";
        $url .= "ver=".$this->getVerison();
        $url .= "&";
        $url .= "timestamp=".$timestamp;
        $url .= "&";
        $url .= "appkey=".$this->getAppkey();
        $url .= "&";
        $url .= "signature=".$this->getSign();

        $data = json_encode($data);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        // POST数据

        curl_setopt($ch, CURLOPT_POST, 1);

        // 把post的变量加上
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json; charset=utf-8',
                'Content-Length: ' . strlen($data)
            )
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }
}