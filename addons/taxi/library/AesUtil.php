<?php
/*
@auth:raven
@date:2021/9/28 19:09
@remark:
*/

namespace addons\taxi\library;

use think\Log;

class AesUtil
{
    /**
     * AES key
     *
     * @var string
     */
    private $aesKey;
    private $mchid = "1608007797";
    private $serial_no = "6E469BBC2117693AA9853F2D98E4C661D42BE37E";

    const KEY_LENGTH_BYTE = 32;
    const AUTH_TAG_LENGTH_BYTE = 16;

    /**
     * Constructor
     */
    public function __construct($aesKey)
    {
//        if (strlen($aesKey) != self::KEY_LENGTH_BYTE) {
//            throw new InvalidArgumentException('无效的ApiV3Key，长度应为32个字节');
//        }
        $this->aesKey = $aesKey;
    }

    /**
     * Decrypt AEAD_AES_256_GCM ciphertext
     *
     * @param string    $associatedData     AES GCM additional authentication data
     * @param string    $nonceStr           AES GCM nonce
     * @param string    $ciphertext         AES GCM cipher text
     *
     * @return string|bool      Decrypted string on success or FALSE on failure
     */
    public function decryptToString($associatedData, $nonceStr, $ciphertext)
    {
        $ciphertext = \base64_decode($ciphertext);

        if (strlen($ciphertext) <= self::AUTH_TAG_LENGTH_BYTE) {

            return false;
        }

        // ext-sodium (default installed on >= PHP 7.2)
        if (function_exists('\sodium_crypto_aead_aes256gcm_is_available') &&
            \sodium_crypto_aead_aes256gcm_is_available()) {
            echo 1;

            return \sodium_crypto_aead_aes256gcm_decrypt($ciphertext, $associatedData, $nonceStr, $this->aesKey);
        }

        // ext-libsodium (need install libsodium-php 1.x via pecl)
        if (function_exists('\Sodium\crypto_aead_aes256gcm_is_available') &&
            \Sodium\crypto_aead_aes256gcm_is_available()) {
            echo 2;
            return \Sodium\crypto_aead_aes256gcm_decrypt($ciphertext, $associatedData, $nonceStr, $this->aesKey);
        }

        // openssl (PHP >= 7.1 support AEAD)
        if (PHP_VERSION_ID >= 70100 && in_array('aes-256-gcm', \openssl_get_cipher_methods())) {
            echo 3;
            $ctext = substr($ciphertext, 0, -self::AUTH_TAG_LENGTH_BYTE);
            $authTag = substr($ciphertext, -self::AUTH_TAG_LENGTH_BYTE);

            return \openssl_decrypt($ctext, 'aes-256-gcm', $this->aesKey, \OPENSSL_RAW_DATA, $nonceStr,
                $authTag, $associatedData);
        }

        throw new \RuntimeException('AEAD_AES_256_GCM需要PHP 7.1以上或者安装libsodium-php');
    }

    public static function getEncrypt($str)
    {
        //$str是待加密字符串
        $public_key_path = ROOT_PATH . '/addons/epay/certs/shanghu_cert.crt';
        $public_key = file_get_contents($public_key_path);

        $encrypted = '';
        if (openssl_public_encrypt($str, $encrypted, $public_key, OPENSSL_PKCS1_OAEP_PADDING)) {
            //base64编码
            $sign = base64_encode($encrypted);
        } else {
            throw new Exception('encrypt failed');
        }
        return $sign;
    }


    function getSign($content)
    {
        $private_key_path = ROOT_PATH . '/addons/epay/certs/apiclient_key.pem';
        $privateKey = file_get_contents($private_key_path);
//        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
//            wordwrap($privateKey, 64, "\n", true) .
//            "\n-----END RSA PRIVATE KEY-----";

        $key = openssl_get_privatekey($privateKey);
        openssl_sign($content, $signature, $key, 'sha256WithRSAEncryption');
        openssl_free_key($key);
        $sign = base64_encode($signature);
        return $sign;
    }

    function getAuth($sign,$time,$nonce)
    {
        $auth = 'Authorization: WECHATPAY2-SHA256-RSA2048 mchid="'. $this->mchid.'",nonce_str="'.  $nonce.'",signature="'.$sign .'",timestamp="'. $time  .'",serial_no="'.$this->serial_no.'"';
        return $auth;
    }
}