<?php

namespace addons\taxi\model;

use addons\swain\model\Swain;

class TianmaiOrder extends Swain
{
    protected $name = "taxi_tianmai_order";


    public static function addInfo($params)
    {
        extract($params);

        $data['car_number'] = $hphm;
        $data['car_color'] = $hpys;
        $data['drvier_number'] = $cyzgzh;
        $data['user_up_car_time'] = strtotime($ckscsj);
        $data['user_up_car_latitude'] = $wd1;
        $data['user_up_car_longitude'] = $jdl;
        $data['user_down_car_time'] = strtotime($ckxcsj);
        $data['user_down_car_latitude'] = $wd2;
        $data['user_down_car_longitude'] = $js2;
        $data['operating_mileage'] = $yylc;
        $data['deadhead_mileage'] = $kslc;
        $data['price'] = $yyje;

        self::create($data);

    }

}