<?php
/*
@auth:raven
@date:2021/9/14 20:33
@remark:
*/

namespace addons\taxi\model;

use addons\golden\library\Golden;
use addons\swain\model\Swain;
use think\Exception;
use think\Model;
use traits\model\SoftDelete;
use think\Db;

class Invoice extends Swain
{
    use SoftDelete;

    protected $name = "taxi_invoice";
    protected $autoWriteTimestamp = true;
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    /**
     * @name:开票
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/14 20:34
     */
    public static function openInvoice($params)
    {
        $result = false;
        Db::startTrans();
        try {

            extract($params);
            $golden = Golden::instance();
            $url = "/invoice/easy_invoice";
            $invoice = self::get(['trade_no'=>$trade_no]);

            if($invoice)
            {
                if($invoice->status == 1)
                {
                    return false;
                }
            }

            if(!$invoice)
            {
                $current_time = strtotime($current_time);
                $invoice_data = compact('driver_uid','driver_job_no','trade_no','passenger_uid','trade_amount','current_time');
//                var_dump($invoice_data);
                self::create($invoice_data);
                $invoice = self::get(['trade_no'=>$trade_no]);
            }

            $data['seller_taxpayer_num'] = $seller_taxpayer_num;
            $data['title_type'] = $title_type + 1;
            $data['buyer_title'] = $buyer_title;
            $data['order_id'] = "SN".time().rand(1000000,99999999);
            $data['total_price_has_tax'] = $trade_amount*100;
            $data['callback_url'] = "";
            $data['buyer_email'] = $buyer_email;

            $res = $golden->httpRequest($url,$data);

            $res = json_decode($res,true);

            $invoice_id = $res['data']['invoice_id'];
            $order_sn = $res['data']['order_sn'];

            $invoice->invoice_id = $invoice_id;
            $invoice->order_sn = $order_sn;
            $invoice->status = 1;
            $invoice->invoicetime = time();
            $invoice->save();

            $result = true;
            Db::commit();

        }
        catch (Exception $e)
        {
            $result = false;
            DB::rollback();
        }

        return $result;

    }

    /**
     * @name:开票申请
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/23 15:31
     */
    public function standardApply()
    {
        $resultMsg = "申请成功";
        $resultCode = "APPLY_SUCCESS";



    }

}