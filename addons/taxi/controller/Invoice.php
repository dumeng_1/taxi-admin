<?php
/*
@auth:raven
@date:2021/9/14 20:34
@remark:
*/

namespace addons\taxi\controller;

use addons\swain\controller\Swain;
use addons\taxi\model\Invoice as InvoiceModel;
use app\common\controller\Api;


class Invoice extends Swain
{
    protected $noNeedRight = ['*'];
    protected $noNeedLogin = ['*'];
    public function openInvoice()
    {
        $params = $this->request->post();
        $result = InvoiceModel::openInvoice($params);

        if($result)
        {
            $this->success('开票成功,已发送至指定邮箱');
        }
        else
        {
            $this->error("系统错误");
        }
    }
}