<?php
/*
@auth:raven
@date:2021/9/28 16:23
@remark:
*/

namespace addons\taxi\controller;

use addons\epay\library\Service;
use addons\swain\controller\Swain;
use addons\taxi\library\AesUtil;
use addons\wechatv3\library\WeChatV3;
use think\Log;
use Yansongda\Pay\Gateways\Wechat\Support;


use Yansongda\Pay\Pay;

class WxNotify extends Swain
{

    public function index()
    {
        $wechat = new WeChatV3();

        $serial_no = "21A02B8AA97B87A96D3A1DA1DBEFD8034DA52672";

        $data['driver_name'] = $wechat->getEncrypt("陈花");
        $data['id_card_number'] =  $wechat->getEncrypt('412702198810196582');
        $data['company_name'] = "河南德鸿科技有限公司";
        $data['mchid'] = "1608007797";
        $data['driver_license'] = "6298416";
        $data['driver_category'] = "MAIN";
        $data['driver_status'] = 'ON_DUTY';
        $data['region_id'] = '410105';

        $time = time();
        $nonce = $wechat->getNonce();
        $body = "";

        //$sign_str = $wechat->getSignStr('POST','/v3/taxi-invoice/driver/update-driver',$time,$nonce,$body);
        $sign_str = $wechat->getSignStr('GET','/v3/certificates',$time,$nonce,$body);
        $sign = $wechat->getSign($sign_str);

        $auth = $wechat->getAuthorization($sign,$time,$nonce);
        $wechatpay_serial = $wechat->getWechatpaySerial();
        $header[] = $auth;

//        $header[] = $wechatpay_serial;

//        $url = "https://api.mch.weixin.qq.com/v3/taxi-invoice/driver/update-driver";
        $url = "https://api.mch.weixin.qq.com/v3/certificates";

        $result = $wechat->requestUrl($url,$data,$header);

        $result = json_decode($result,true);

        return json($result);

    }

    public function test()
    {
        $config = Service::getConfig('wechat');
        $pay = Pay::wechat($config);

        $mchid = "1608007797";
        $serial_no = "6E469BBC2117693AA9853F2D98E4C661D42BE37E";

        $method = 'GET';
        $url = '/v3/certificates';
        $time = time();
        $nonce = md5(time());
        $body = '';
        $sign_stra = "$method\\n$url\\n$time\\n$nonce\\n$body\\n";
        $sign_str = "$method\n$url\n$time\n$nonce\n$body\n";
        $sign = $this->getSign($sign_str);

        $auth = 'Authorization: WECHATPAY2-SHA256-RSA2048 mchid="' . $mchid . '",nonce_str="' . $nonce . '",signature="' . $sign . '",timestamp="' . $time . '",serial_no="' . $serial_no . '"';

        $header = [
            $auth,
            'User-Agent:Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/93.0.4577.82'
        ];
        $Url = "https://api.mch.weixin.qq.com/v3/certificates";

        $result = $this->http_request($Url, [], $header);



        $this->success("", $result);

        //return json($header['Authorization']);

        //410105


//        $data['driver_name'] = $this->getEncrypt('陈花');;
//        $data['id_card_number'] = $this->getEncrypt('412702198810196582');
//        $data['company_name'] = '河南德鸿科技有限公司';
//        $data['mchid'] = '1608007797';
//        $data['driver_license'] = "6298416";
//        $data['driver_category'] = 'MAIN';
//        $data['driver_status'] = 'ON_DUTY';
//        $data['region_id'] = '410105';

//        $data['plate_number'] = "豫AD31281";
//        $data['company_name'] = "河南德鸿科技有限公司";
//        $data['region_id'] = "410105";
//        $data['invoice_flag'] = "INVOICE";

//        return json($data);

//        $url = "https://api.mch.weixin.qq.com/v3/taxi-invoice/taxi/update-taxi";
//
//


//        $result = Support::generateSign($data);
//        return json($result);
//        $result = $this->getEncrypt($data['driver_name']);
//        var_dump($result);

//        return json($result);

    }

    public function jiemi()
    {
        $aesKey = "dcf1493df2e967eb062a03cfded18b16";
        $nonceStr = "a7624e9227b9";
        $ciphertext = "tMPXJiNeGx8T4ze8y09ZrhQ2JRvbmUXdXtuFHN2qceGZzF/jREEUGHKm3Uzz4KnZOR5eIoYhNs6DXwYaa+ppNGoLwFl46IVUZ+kNzD9DzPrNdX9n31TziAvDpBkQKAzSomgygtCeqF3OfFfYyKMRY5g0vCzbroK4IrArLQm7vVb+HiTCSl4EEykaaYcru+wV6dddvdQeX4TPem6ospcUHNHhjQE1mWPtp8Hpt2AEKNsfB7IOT3donSKS8Vd76b9HlDJHSnCj182H9mFsgaUNYQJUIYJfcmp+NFbaJ6RFS15tseSZ3/yzIPhkkPt3ebeQdRC1P2khsqkZmZqjdq9UR2A1E3HOGWmgJhaz9GJVBq7V/0kceQ5P2I9lI1LdLB9o/WfMU6+rBjA6P/ifjr9HLj8MBMJ3YmIcVRcE97oNApgnGr6Qq/k1E9jrKj/5ojRYM8Bh7+RNcq/pfuIXjO50gWBSHbsIAZIK/P2HJc/i7rpgJfwM5IBSpxJVM75u+QidAmo9xyS2x5o2KHEKBikyr1FqioF+frO+giWYRepuDtm0SJyNswDPJAryxoP2gQ+657UTutMhQG8pLv+PMrLpKP06G0/JFt9fIgMwm2kz+T7xhVEhFdY873PCuNixc2yM9IiSwEu65lEi99JAZYviuK4K4qpbmXFVT/+WEaZgYc83TWyqPXiaCP1w6jJgTkkWHIBSkG+hmvAd6EvpvOCbzboVLJ2nB3ETB6O/g4iLizff78vdj6hMGpQr7t24bX/B/pYPly/qMQj05Xr5/vKfPIxvo26I7Pa86R9u0fPM+BDtK9XEo8NJkBcXUcYPpjJHBx5tr0aB0uAT9f5XC1+CPky6SAB6MiLO/9FcUNePGPH528OiPdZEzxrWYpZywY7mYpk4FoceH4+5mHfXGru8bhCbVuFdyI7sdw0HHOTP4KDXV+y0wcagDp3RRUdhMhAVtg9CXKUT6lV/C/6Qtap1qfI4hzBfVC9ax0P3CVxhOaIOlu/JmVmTFcVyp3KVyGRNp6wBYQY7IGDL/cX2qDXOZn6vEP8zrgFB4xb1FSYvghfoT5GZbZ7b/ROTiOfCrIjqp82XTZxs4+FQ6gAIBI6KBcR7u5iQ2xXlIMcFCjzqkplJ3Ju4KNdc6yqVio2ZxptxcGDkvslKIH6xsyrAPe3ThWHOWxLN6mKS7MmiUWup6pSsnXAYUs1LaNJwqUClU13uIrhDpskjHWK5vJnvHAI3D8jjvOnlqJNw7+40/oXyyHLSmLMLUif4HOl9YTamVIXU2LsJSN9UQX/JoeGIn6fQRs1KRExEJajofAeQ/rJTJJwysWIfudlf/QJMozY9R8fuXVc2/8dqV89F76Qs4qIPHMvUlTSIa9FoEmgZD6zdyFQfdP+bBG+rWuIU4Gq96bgT6NdO1huJ8CgfqXOVDLsSJ4vZZ2ruLjLIvjyIkAuvVjvgyrjkmZ0QojTgizR+Bf4IgK6BEb4jMN5alXLGH1EHPWLACHGK/DeXP4P3fpPBrZ1y4WDn/EunmScmlysgqW9szSzE3OIrnCaBTr2pRymCsJB/0gzeeBCjMD6mKsddRGN11yUrJZ73c9Zq2HwfMASkC8JQqmSam7+/9Es/u2cd9YIMRvARNaRO7OjzC0Kk7t8GUagqZxFLJ34IX3zZfYaOxBcLdRmlJ7WWUsonW2O1KwiJ4SqF+VktAay2U5LkZltK4WQigKKBcr0POLTP8w4w532BYTYBWA1iGaxUja+tKW73vFGPZblNd3y1vMtbYeNdYATYyg51ruYccAw0exNGM6Oo6dr6KPMtomkM4q61zsucij29z9NqVM0SJTCOnodQKNtLF65DRrm+Zr3p6fk3iMXKxWucmY9hZP2QyDPy+URBeKqD0Q==";
        $associatedData = "certificate";

        $aesUtil = new AesUtil($aesKey);
        $result = $aesUtil->decryptToString($associatedData,$nonceStr,$ciphertext);
        var_dump($result);

    }


    public function addDrvier()
    {
        $serial_no = "21A02B8AA97B87A96D3A1DA1DBEFD8034DA52672";

        $data['driver_name'] = AesUtil::getEncrypt("陈花");
        $data['id_card_number'] = AesUtil::getEncrypt('412702198810196582');
        $data['company_name'] = "河南德鸿科技有限公司";
        $data['mchid'] = "1608007797";
        $data['driver_license'] = "6298416";
        $data['driver_category'] = "MAIN";
        $data['driver_status'] = 'ON_DUTY';
        $data['region_id'] = '410105';

        $Url = "https://api.mch.weixin.qq.com/v3/taxi-invoice/driver/update-driver";

//        $Url = "https://api.mch.weixin.qq.com/v3/certificates";

//        $method = 'GET';
//        $url = '/v3/certificates';

        $method = 'POST';
        $url = '/v3/taxi-invoice/driver/update-driver';
        $time = time();
        $nonce = md5(time());
        $body = json_encode($data,JSON_UNESCAPED_UNICODE);

        $body = str_replace("\\/", "/", $body);

        $body = "{name:1}";

//        $body = "";

        $sign_stra = "$method\\n$url\\n$time\\n$nonce\\n$body\\n";

        $sign_str = "$method\n$url\n$time\n$nonce\n$body\n";

        $aes = new AesUtil("");

        $sign = $aes->getSign($sign_str);

        Log::write($sign);

        $auth = $aes->getAuth($sign,$time,$nonce);;

        $header = [
            $auth,
            'Wechatpay-Serial:'.$serial_no,
            'Content-Type:application/json',
            'User-Agent:Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/93.0.4577.82'
        ];

        $result = $this->http_request($Url,$data,$header);

        $this->success('',$result);
    }


}