<?php
/*
@auth:raven
@date:2021/9/3 18:07
@remark:
*/

namespace addons\taxi\controller;


use addons\epay\library\Service;
use addons\golden\library\Golden;
use addons\swain\controller\Swain;
use addons\taxi\model\Invoices;
use app\common\controller\Api;
use think\Log;
use Yansongda\Pay\Gateways\Alipay\Support;
use Yansongda\Pay\Pay;
use addons\taxi\model\Alipay as AlipayModel;
use addons\taxi\model\Invoice as InvoiceModel;

class Notify extends Swain
{

    const SUCCESS = '10000';
    const FAIL = '40004';


    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    public function driveGet()
    {
        $params = $this->request->param();
        $config = Service::getConfig('alipay');
        $pay = Pay::alipay($config);

        $result = [];

        $code = 10000;
        $msg = "Success";
        $biz_no = $params['biz_no'];
        $company_pid = "2021002133635801";
        $driver_match=true;
        $driver_phone=13911112222;
        $city_code=310000;

        $result['response'] = compact('code','msg','biz_no','company_pid');
        //$sign = Support::generateSign($result['response']);
        //$result['sign'] = $sign;

        return json($result);

    }

    public function receiptQuery()
    {
        $params = $this->request->param();

        $config = Service::getConfig('alipay');
        $pay = Pay::alipay($config);


        // Log::write($params);
        // AlipayModel::add($result);
        // dump($result);exit;
        // $sign = Support::generateSign($result['response']);
        // $result['sign'] = $sign;
        // Log::write($params);
        // die();

        // $invoice = InvoiceModel::get(['biz_no'=>$params['biz_no']]);
        $receipt_url = "/pages/home/invoice/invoice";
        $code = 10000;
        $msg = "Success";
        $biz_no = $params['biz_no'];
        $company_pid = "2021002133635801";
        $driver_uid = $params['driver_uid'];
        $receipt_open = true;

        $result['response'] = compact('code','msg','biz_no','company_pid','driver_uid','receipt_open','receipt_url');

        // $config = Service::getConfig('alipay');
        // $pay = Pay::alipay($config);

        // $code = 10000;
        // $msg = "Success";
        // $biz_no = time();
        // $company_pid = "2021002133635801";
        // $driver_uid = $params['driver_uid'];
        // $receipt_open = true;
        // $receipt_url = "/pages/home/home"; //小程序路径


        // AlipayModel::add($result);
        // dump($result);exit;
        $sign = Support::generateSign($result['response']);
        $result['sign'] = $sign;

        return json($result);
    }

    public function tradeinfoSend()
    {
        $config = Service::getConfig('alipay');
        $pay = Pay::alipay($config);

        $result = [];

        $code = 10000;
        $msg = "Success";
        $biz_no = "12345678";

        $result['response'] = compact('code','msg','biz_no');
        //$sign = Support::generateSign($result['response']);
        //$result['sign'] = $sign;

        return json($result);
    }

        /**
     * @name:发票申请
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/7 15:33
     */


            /**
     * @name:发票申请
     * @return:
     * @remark:
     * @auth:raven
     * @datetime:2021/9/7 15:33
     */
    public function standardApply()
    {
        /*
         *  resultMsg	String	业务结果说明  APPLY_SUCCESS  INVOICE_PARAM_ILLEGAL
            resultCode	String	业务结果码    申请成功  开票参数非法
            resultUrl	String	用户提交申请之后，支付宝侧会提供入口让用户访问ISV 提供的结果页面（可选，链接需进行 urlencode）。
         * */


        $config = Service::getConfig('alipay');
        $pay = Pay::alipay($config);

        $resultCode = "APPLY_SUCCESS";
        $resultMsg = "申请成功";
        $resultUrl = "";

        $result = compact('resultCode','resultMsg','resultUrl');
        $sign = Support::generateSign($result);
        $result['sign'] = $sign;


        // $code = 10000;
        // $msg = "申请成功";

        // $result['response'] = compact('code','msg');

        return json($result);
    }

    /**
     * 接收响应 receiptResponse
     * */
    public function receiptResponse()
    {
        $this->success("成功");
    }


    public function tianmaiTaxiData()
    {
        $params = $this->request->params();

        \addons\taxi\model\TianmaiOrder::create($params);

    }

    public function alipayInvoiceNotify()
    {
        $params = $this->request->param();
        extract($params);

        /*
          'payerTelPhone' => '',
          'orderNo' => '16339377458012411222214',
          'sign' => 'P38hWcOPoaX8aqkPToWiDt5%2BtZedjcIP2Yc5o40mN4FGgmiHAhofN14DNtXKADXjZtxd6mL1FcpcJYmnPT6cMvg%2FDd0x5wyXyMiAMvmWCLu%2FPTSQ62gaKInbvCCnRzKU7B4DlX%2FALvZ%2BerJyU2umXHsXwQNeVzhN9DJHtAC%2Bj4NkBF8wP7ztxsl3FXemlsZYu%2B57gftYBT7P4VnX5gTYwHkKgXCVutl9dp6A%2B9p9hJaUKOmfk9QPpmtLHYKcC2lzYNFXi38Uiq5kLXPOj2PBB9Wxi1n8QbnQD8TR4p3nxCrrDOidrq9mku%2BL9zaW8hUWnIKBAxFrODE2Oa2egIdtzw%3D%3D',
          'mShortName' => '2088602007137532',
          'payerBankName' => '',
          'invoiceAmount' => '0.01',                    金额
          'payerBankAccount' => '',
          'userId' => '2088012411222214',               用户uid
          'userMobileNo' => '18623770016',              用户手机
          'applyId' => '2021101100152005210003556957',  提交单号
          'tradeTime' => '2021-10-11 15:35:46',
          'payerAddress' => '',
          'subShortName' => '2088602007137532',         收款人uid
          'payerName' => '河南德鸿供应链管理有限公司',  抬头
          'payerRegisterNo' => '91410105MA464CC51D',    税号
          'userEmail' => '839113670@qq.com',            用户邮箱
          'isRetryOpen' => false,
        */
        $info = Invoices::get(['order_no'=>$orderNo]);
        if(!$info)
        {
            $data['payer_tel_phone'] = $payerTelPhone;
            $data['order_no'] = $orderNo;
            $data['m_short_name'] = $mShortName;
            $data['payer_bank_name'] = $payerTelPhone;
            $data['invoice_amount'] = $invoiceAmount;
            $data['payer_bank_account'] = $payerBankAccount;
            $data['user_id'] = $userId;
            $data['user_mobile_no'] = $userMobileNo;
            $data['apply_id'] = $applyId;
            $data['trade_time'] = $tradeTime;
            $data['payer_address'] = $payerAddress;
            $data['sub_short_name'] = $subShortName;
            $data['payer_name'] = $payerName;
            $data['payer_register_no'] = $payerRegisterNo;
            $data['user_email'] = $userEmail;
            $data['is_retry_open'] = $isRetryOpen;

            Invoices::create($data);
            $info = Invoices::get(['order_no'=>$orderNo]);
        }


        //用高登开票
        if($info->status == 0)
        {

            $url = "/invoice/easy_invoice";
            $golden = Golden::instance();
            $golden_data['seller_taxpayer_num'] = '140301195503104110';
            $golden_data['title_type'] = 2;
            $golden_data['buyer_title'] = $payerName;
            $golden_data['buyer_taxpayer_num'] = $payerRegisterNo;
            $golden_data['order_id'] = "SN".time().rand(1000000,99999999);
            $golden_data['total_price_has_tax'] = $invoiceAmount*100;
            $golden_data['callback_url'] = cdnurl('addons/taxi/goldenNotify');
            $golden_data['buyer_email'] = $userEmail;
            $golden_data['callback_url'] = cdnurl('/addons/taxi/notify/goldenAlipayNotify',true);


            $res = $golden->httpRequest($url,$golden_data);
            $res = json_decode($res,true);
            $info->invoice_order_id = $golden_data['order_id'];
            $info->invoice_order_sn = $res['data']['order_sn'];
            $info->status = 1;

            $info->save();
        }

        /*
          resultMsg	String	业务结果说明
        resultCode	String	业务结果码
        resultUrl	String	用户提交申请之后，支付宝侧会提供入口让用户访问ISV 提供的结果页面（可选，链接需进行 urlencode）。
        sign	String	签名
         * */

        vendor('aop.AopClient');
        vendor('aop.request.AlipayEbppInvoiceInfoSendRequest');
        vendor('aop.AopCertClient');

        $aop = new \AopClient();

        $aop->rsaPrivateKey = "MIIEpQIBAAKCAQEApdfdqaUlcU2hj6/4aDlQPdIaAU/rKkIq23GKOZeRjtxicgfnlMnsIeFA/cPMdaWhxVhjascj+R1Ho2Qt49BOqNq1M3oAx/KbtIrkGd4J/NIUN31620oIUelPwNRHdUfxqsRPIEvxMKN7BzvpkYxsT5IO2U3ab0ws+d9qf2Vtit5rbLoPct+wgAsbKwI3y0qm98QaN+obIfj7L//Vp2vgkO55Fx2JxHB5LDMKMZQ6hhmYMg6nDSxmF8H75v+pLV93j8tFOHmaQyPRBl1TmDLD3WjED6Y47xBkl1KkzjT3Edu+G0K+WMgPGRAdYT3cyK5xobzTlZijtmwykMXIPa9QlwIDAQABAoIBAQCeJoHywlM3fmGAZb4MngNAQPidobnP3bOC+v5mt3yOcOnMdm9IO1rcYDObC7oRwvbG++gqrmdGq75iOSlt43rvmUlNIp5WDqyfegfWByEdILU4yYd8UVgnzMdKSuUuGYKDZ5tRYDpMOMoc9uxg6TlslAtm/NA+bLVDP+QOQwo4byGPXVzAQ06SdgcTPIPgRChMu71ELqHz/8VJfqdwZIsVz4DFoZO3qbw8gorFk+CbNyplWw6A14hh5ni/Qb3tUfnBGqH+77rprrLbJUT1j182mHqtRNIvDAmAHTz+6E9ieQSh2mm2DLz0jidM+yYf8M9iQH/+q1gCsMOuwm1l/JgpAoGBAORc9OIaGZNuIJaPevTkDH1k9nDklcTG2tyA55Fc8y4KAAZeGHLkWcRlGB2jz9i0UbX5XU1ZVYgw+YhcITKvRACySRYTmsWjL7RUUJi8USe9fZRvtzspiGFid3OexT5yiTcdu4/xwMKVdWPBsDAXp7GuTJprag4bG3HwBS9A025FAoGBALnp8j42XIr/GH/sq+4WnX2pHolnd32GeAWid2naU2yTKyB2XXnE0pMU2QkakS7Yk8s/cxv5JZYqyNmhBOvaPDim42UKGUvTmI08LEdE36cedz8E739Tn5RtpZ8igvwBdAvKIPh8hgTQJDGsjYsGRcffaqjfF9wp3jvKTLL9Gs8rAoGBAKSzkn7cOiQF4oUUeKbVL1jlD4T+qDIjBcjRQ7Koqk9DMR9mkBWVMlUeYJVIB2kRLHmlnzBhRPoT8HTzlZH7rH0gGOxOOWtftdFGmlZ+Q734wfDj3fBSlSU/ok2GblYKv4I79IHt54uvxdmr4UneVbklr+QToosH6/RyGxsokxVNAoGACVyCWieamVUnAqoELkTtQTqW0wxms+dle8MmiCn6MWlnrobHRi5m/Aj8tLylutok9wMG5M2y2tDktDCrcsTWa3Pb12aex3asI9B32k7ZhCzAjGfPN3YafvrWcCDov4/DLCTNbDW4+d0RNX8e0XVLZjkVwdMZ/HgPPKt/GTQteWkCgYEAjno7OA5TpgNuBeLSehIJlcghNZfQQzsxBjC1HuTDi9wvHy2SmiYnXW3K5ZlpmFZRfRbq6rXlHbcqNrGVjusG0uzvcYYluwuwwFikM1j1RabEHdbYebjkys1BBddKoXMHhmGyL63KqpnDmgDehLZlsycQ+oaAM3qs8ycbhHLt5Bo=";
        $aop->alipayrsaPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlqesLO+yf/VUuAsneQTAJSL+5TzVdlddXr1a+Z9SxcJYIyk8H4bKbVDLOSHFuogVBMhVmCnuC4Y25RWU5BOQsOcCw2TyKkfZ4YV713ZxixIr02Xz82AqiHIv5MpeMMLlyUpm9RUbJTSwLMoBt14zgJ/Fccyv5WdlBCd0Dwm4ofw7+Dwcio/ImEO33o6qGnvECh1Ls8iPTen0Hbqo2FpoVFQ3G0WG/lQ++TfD0LX1YDjnW+9tOTAey9IiEZP7ZiB9Gfi1x+xlPbNB9DqU8JFYoyXM9dqTULhEZqHKQa8PEd2exWWIKDJGGjLCdgy7OhWyZWY+wr4s0R4in+G9ilPSXQIDAQAB";


        $result['resultMsg'] = "申请成功";
        $result['resultCode'] = "APPLY_SUCCESS";
        $result['sign'] = $aop->generateSign($result,'RSA2');
        return json($result);

//        Log::write("alipayInvoiceNotify");
//        Log::write($params);
    }

    public function goldenAlipayNotify()
    {
        $params = $this->request->post();

        /*
         'appkey' => 'BWFnG6FICTlrASLfefRcY2ORA2GxCfdM',
          'check_code' => '15401999065136609234',
          'cipher_text' => '037/+0210*1306*9**158332+0+360/08/391594**&gt;012-7+192/5140/tgt     ;&gt;9',
          'g_unique_id' => '6855050378656922879',
          'message' => '成功',
          'notify_time' => '2021-10-16 16:02:51',
          'notify_type' => 'invoice.blue',
          'order_id' => 'SN1634371370',
          'order_sn' => '6855050378656922879',
          'pdf_url' => 'https://view-proxy-dev.wetax.com.cn/center/prod_make/20211016/invoice_901634371370_34371370_a20cf2703ca04a55a7dce5e5a47a58c0.pdf',
          'qrcode' => '',
          'ticket_code' => '901634371370',
          'ticket_date' => '2021-10-16 16:02:51',
          'ticket_sn' => '34371370',
          'ticket_status' => '2',
          'ticket_tax_amount' => '0',
          'ticket_total_amount_has_tax' => '0.01',
          'ticket_total_amount_no_tax' => '0.01',
         * */

        Log::write("goldenAlipayNotify");
        Log::write($params);

        extract($params);

        //找到订单
        $invoice = Invoices::get(['invoice_order_sn'=>$order_sn]);
        $invoice->status = 2;
//        $invoice->invoice_order_sn = $order_sn;
        $invoice->invoice_ticket_code = $ticket_code;
        $invoice->invoice_ticket_sn = $ticket_sn;
        $invoice->invoice_ticket_date = $ticket_date;
        $invoice->invoice_ticket_tax_amount = $ticket_tax_amount;
        $invoice->invoice_ticket_total_amount_has_tax = $ticket_total_amount_has_tax;
        $invoice->invoice_ticket_total_amount_no_tax = $ticket_total_amount_no_tax;
        $invoice->invoice_pdf_url = $pdf_url;
        $invoice->save();

        $config = Service::getConfig('alipay');
        $pay = Pay::alipay($config);



        vendor('aop.AopClient');
        vendor('aop.request.AlipayEbppInvoiceInfoSendRequest');
        vendor('aop.AopCertClient');

        $aop = new \AopClient();


        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->appId = '2021002133635801';
        $aop->rsaPrivateKey = "MIIEpQIBAAKCAQEApdfdqaUlcU2hj6/4aDlQPdIaAU/rKkIq23GKOZeRjtxicgfnlMnsIeFA/cPMdaWhxVhjascj+R1Ho2Qt49BOqNq1M3oAx/KbtIrkGd4J/NIUN31620oIUelPwNRHdUfxqsRPIEvxMKN7BzvpkYxsT5IO2U3ab0ws+d9qf2Vtit5rbLoPct+wgAsbKwI3y0qm98QaN+obIfj7L//Vp2vgkO55Fx2JxHB5LDMKMZQ6hhmYMg6nDSxmF8H75v+pLV93j8tFOHmaQyPRBl1TmDLD3WjED6Y47xBkl1KkzjT3Edu+G0K+WMgPGRAdYT3cyK5xobzTlZijtmwykMXIPa9QlwIDAQABAoIBAQCeJoHywlM3fmGAZb4MngNAQPidobnP3bOC+v5mt3yOcOnMdm9IO1rcYDObC7oRwvbG++gqrmdGq75iOSlt43rvmUlNIp5WDqyfegfWByEdILU4yYd8UVgnzMdKSuUuGYKDZ5tRYDpMOMoc9uxg6TlslAtm/NA+bLVDP+QOQwo4byGPXVzAQ06SdgcTPIPgRChMu71ELqHz/8VJfqdwZIsVz4DFoZO3qbw8gorFk+CbNyplWw6A14hh5ni/Qb3tUfnBGqH+77rprrLbJUT1j182mHqtRNIvDAmAHTz+6E9ieQSh2mm2DLz0jidM+yYf8M9iQH/+q1gCsMOuwm1l/JgpAoGBAORc9OIaGZNuIJaPevTkDH1k9nDklcTG2tyA55Fc8y4KAAZeGHLkWcRlGB2jz9i0UbX5XU1ZVYgw+YhcITKvRACySRYTmsWjL7RUUJi8USe9fZRvtzspiGFid3OexT5yiTcdu4/xwMKVdWPBsDAXp7GuTJprag4bG3HwBS9A025FAoGBALnp8j42XIr/GH/sq+4WnX2pHolnd32GeAWid2naU2yTKyB2XXnE0pMU2QkakS7Yk8s/cxv5JZYqyNmhBOvaPDim42UKGUvTmI08LEdE36cedz8E739Tn5RtpZ8igvwBdAvKIPh8hgTQJDGsjYsGRcffaqjfF9wp3jvKTLL9Gs8rAoGBAKSzkn7cOiQF4oUUeKbVL1jlD4T+qDIjBcjRQ7Koqk9DMR9mkBWVMlUeYJVIB2kRLHmlnzBhRPoT8HTzlZH7rH0gGOxOOWtftdFGmlZ+Q734wfDj3fBSlSU/ok2GblYKv4I79IHt54uvxdmr4UneVbklr+QToosH6/RyGxsokxVNAoGACVyCWieamVUnAqoELkTtQTqW0wxms+dle8MmiCn6MWlnrobHRi5m/Aj8tLylutok9wMG5M2y2tDktDCrcsTWa3Pb12aex3asI9B32k7ZhCzAjGfPN3YafvrWcCDov4/DLCTNbDW4+d0RNX8e0XVLZjkVwdMZ/HgPPKt/GTQteWkCgYEAjno7OA5TpgNuBeLSehIJlcghNZfQQzsxBjC1HuTDi9wvHy2SmiYnXW3K5ZlpmFZRfRbq6rXlHbcqNrGVjusG0uzvcYYluwuwwFikM1j1RabEHdbYebjkys1BBddKoXMHhmGyL63KqpnDmgDehLZlsycQ+oaAM3qs8ycbhHLt5Bo=";
        $aop->alipayrsaPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlqesLO+yf/VUuAsneQTAJSL+5TzVdlddXr1a+Z9SxcJYIyk8H4bKbVDLOSHFuogVBMhVmCnuC4Y25RWU5BOQsOcCw2TyKkfZ4YV713ZxixIr02Xz82AqiHIv5MpeMMLlyUpm9RUbJTSwLMoBt14zgJ/Fccyv5WdlBCd0Dwm4ofw7+Dwcio/ImEO33o6qGnvECh1Ls8iPTen0Hbqo2FpoVFQ3G0WG/lQ++TfD0LX1YDjnW+9tOTAey9IiEZP7ZiB9Gfi1x+xlPbNB9DqU8JFYoyXM9dqTULhEZqHKQa8PEd2exWWIKDJGGjLCdgy7OhWyZWY+wr4s0R4in+G9ilPSXQIDAQAB";
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'utf-8';
        $aop->format = 'json';


        $biz_content = [];

//        $ali_data['app_id'] = "2021002133635801";
//        $ali_data['method'] = 'alipay.ebpp.invoice.info.send';
//        $ali_data['format'] = 'JSON';
//        $ali_data['charset'] = 'utf-8';
//        $ali_data['sign_type'] = 'RSA2';
//        $ali_data['timestamp'] = date('Y-m-d H:i:s');
//        $ali_data['version'] = '1.0';

        $biz_content['m_short_name'] = $invoice->m_short_name;
        $biz_content['sub_m_short_name'] = $invoice->sub_short_name;

        $invoice_info_info['user_id'] = $invoice->user_id;
        $invoice_info_info['invoice_code'] = $invoice->invoice_ticket_code;
        $invoice_info_info['invoice_no'] = $invoice->invoice_ticket_sn;
        $invoice_info_info['invoice_date'] = date('Y-m-d',strtotime($invoice->invoice_ticket_date));
        $invoice_info_info['sum_amount'] = $invoice->invoice_ticket_total_amount_has_tax;
        $invoice_info_info['ex_tax_amount'] = $invoice->invoice_ticket_total_amount_no_tax;
        $invoice_info_info['tax_amount'] = $invoice->invoice_ticket_tax_amount;

        $invoice_content['item_name'] = "餐饮费";
        $invoice_content['item_no'] = "1010101990000000000";
        $invoice_content['row_type'] = 0;
        $invoice_content['item_ex_tax_amount'] = $invoice->invoice_ticket_total_amount_no_tax;
        $invoice_content['item_tax_rate'] = '0.00';
        $invoice_content['item_tax_amount'] = $invoice->invoice_ticket_tax_amount;
        $invoice_content['item_sum_amount'] = $invoice->invoice_ticket_total_amount_has_tax;

        $invoice_title['title_name'] = $invoice->payer_name;
        $invoice_title['payer_register_no'] = $invoice->payer_register_no;

        $invoice_info_info['invoice_title'] = $invoice_title;
        $invoice_info_info['payee_register_no'] = "140301195503104110";
        $invoice_info_info['payee_register_name'] = "开发平台外部专测";
        $invoice_info_info['out_invoice_id'] = $invoice->invoice_order_id;
        $invoice_info_info['file_download_type'] = "PDF";
        $invoice_info_info['file_download_url'] = $invoice->invoice_pdf_url;
        $invoice_info_info['out_trade_no'] =$invoice->invoice_order_sn;
        $invoice_info_info['invoice_type'] = 'BLUE';
        $invoice_info_info['invoice_kind'] = 'PLAIN';

        $invoice_info_info['invoice_content'][] = $invoice_content;
        $biz_content['invoice_info_list'][] = $invoice_info_info;






//        $ali_data['biz_content'] = json_encode($biz_content);


//        $sign = Support::generateSign($ali_data);
//        $ali_data['sign'] = $sign;

//        echo json_encode($biz_content['invoice_info_list']);

        $url = "https://openapi.alipay.com/gateway.do";

//        $result = $this->postUrl($url,$ali_data);
//        $result = json_decode($result,true);



        $request = new \AlipayEbppInvoiceInfoSendRequest ();
        $request->setBizContent(json_encode($biz_content));

        //Log::write(json_encode($biz_content));


        $result = $aop->execute( $request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;

        Log::write("responseNode");
        Log::write($result);

        if(!empty($resultCode)&&$resultCode == 10000){
            $invoice->status = 3;
            $this->error("成功");
        } else {
            $invoice->status = 4;
        }
        $invoice->save();



        $this->error("成功");
    }



}