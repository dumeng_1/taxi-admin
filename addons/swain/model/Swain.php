<?php

namespace addons\swain\model;

use addons\swain\library\SwainException;
use think\Exception;
use think\Model;

use traits\model\SoftDelete;

class Swain extends Model
{
    use SoftDelete;

    protected $autoWriteTimestamp = true;
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    const NORMAL = 'normal';

    protected $append = ['createtime_text','updatetime_text'];

    public static function getList($params = [])
    {
        $result = self::weigh()->select();
        return $result;
    }

    public function scopeWeigh($query)
    {
        return $query->order('weigh desc');
    }

    public function getImageAttr($value,$data)
    {
        $result = cdnurl($value,true);
        return $result;
    }

    public static function addInfo($params)
    {
        $result = self::create($params);
        $result['id'] = $result->getLastInsID();
        return $result;
    }

    public static function setInfo($id,$params)
    {
        $info = self::where('id','=',$id)->find();
        $result = $info->save($params);
        return $result;
    }

    public function getCreatetimeTextAttr($value,$data)
    {
        $result = "";
        if($data['createtime'])
        {
            $result = date('Y-m-d H:i:s',$data['createtime']);
        }
        return $result;
    }


    public function getUpdatetimeTextAttr($value,$data)
    {
        $result = "";
        if($data['updatetime'])
        {
            $result = date('Y-m-d H:i:s',$data['updatetime']);
        }
        return $result;
    }

}