<?php


namespace addons\swain\library;


class SwainHook
{
    public static function register ($addons = '') {
        $hooks_file = ADDON_PATH.$addons.DS.'hooks.php';
        if(file_exists($hooks_file))
        {
            $default = require $hooks_file;

            foreach ($default as $tag => $behavior) {
                // 数组反转 保证最上面的行为优先级最高
                $behavior = array_reverse($behavior);
                foreach ($behavior as $be) {
                    \think\Hook::add( $be, $tag, true);      // 所有行为都插入最前面
                }
            }
        }

    }


}