<?php
/*
@auth:raven
@date:2021/8/23 14:38
@remark:
*/

$menu = [
    [
        'name'    => 'swain/command',
        'title'   => '插件在线命令',
        'icon'    => 'fa fa-map-marker',
        'sublist' => [
            ["name"  => "swain/command/index","title" => "查看"],
            ["name"  => "swain/command/add","title" => "添加"],
            ["name"  => "swain/command/edit","title" => "编辑"],
            ["name"  => "swain/command/del","title" => "删除"],
            ["name"  => "swain/command/multi","title" => "批量更新"],
        ]
    ]
];

return $menu;