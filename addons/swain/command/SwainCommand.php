<?php
namespace addons\swain\command;

use think\Config;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;

class SwainCommand extends Command
{
    protected function configure()
    {
        $this->setName('swain:Curd')
            ->addOption('name', null, Option::VALUE_REQUIRED, '控制器名字', null)
            ->addOption('table', null, Option::VALUE_OPTIONAL, '表名(不带前缀)', null)
            ->addOption('addon_name', null, Option::VALUE_REQUIRED, '插件名字', null)
            ->addOption('path', null, Option::VALUE_OPTIONAL, '路径', null)
            ->setHelp('自动创建插件CURD')
            ->setDescription('自动创建插件CURD');
    }

    protected function execute(Input $input, Output $output)
    {
        $name = ucwords($input->getOption('name') ?: '');
        $table = $input->getOption('table') ?: '';
        $addon_name = $input->getOption('addon_name') ?: '';
        $path = $input->getOption('path') ?: '';

        $table = $this->getTable($table);
        $name = $this->getControllerName($name);
        //创建控制器
        $option = compact('name','table','addon_name');

        $mvc_data = [ 'controller' , 'model' ];

        foreach($mvc_data as $mvc)
        {
            $path_name = ADDON_PATH.$addon_name.DS.$mvc.DS.$path.DS.$name.'.php';
            $this->writeToFile($option,$mvc,$path_name);
        }

    }


    /**
     * 获取模板
     * @param $type
     * @return string
     */
    protected function getStub($type)
    {
        return __DIR__ . '/Addon/stubs/'.$type.'.stub';
    }

    /**
     * 写入文件
     * @param $option
     * @param $file
     */
    protected function writeToFile($option,$name,$pathname = '')
    {
        $stub = file_get_contents($this->getStub($name));
        $search = [];
        $replace = [];

        foreach($option as $key => $item)
        {
            $search[] = "{%$key%}";
            $replace[] = $item;
        }

        $content = str_replace($search, $replace, $stub);
        if (!is_dir(dirname($pathname))) {
            mkdir(strtolower(dirname($pathname)), 0777, true);
        }
        return file_put_contents($pathname, $content);


    }

    public function getTable($table)
    {
//        $result = "";
        $prefix = Config::get('database.prefix');
        $table = str_replace($prefix,'',$table);
//        $table_arr = explode('_',$table);
//
//        foreach($table_arr as $item)
//        {
//            $result.= ucfirst($item);
//        }

        return $table;
    }

    public function getControllerName($name)
    {
        $result = "";

        $arr = explode('_',$name);

        foreach($arr as $item)
        {
            $result.= ucfirst($item);
        }

        return $result;
    }

}