<?php


namespace addons\swain\controller;

use app\common\controller\Api;
use app\common\library\Auth;
use think\Request;

class Swain extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $user = [];
    protected $params = [];
    protected $model = "";

    public function __construct(Request $request = null)
    {
        parent::__construct($request);
        $this->params = $this->request->post();
        $auth = Auth::instance();
        if($auth->id > 0)
        {
            $this->user = $auth->getUser();
        }

    }








}